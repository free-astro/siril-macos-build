# Build SiriL/macOS inside CircleCI

This repository contains files related to SiriL/macOS build using CircleCI and some tips that could help with local development as well.

## Build process description

To build SiriL/macOS we are using this repo, which contains a fork of relevant parts
of the [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) project (`gimp` branch).
Fork adds modules related to SiriL and some gimp-specific patches to GTK.
Currently build is done using CircleCI.

Because CircleCI is not supporting gitlab [yet] there is a [GitHub
mirror](https://github.com/lock042/siril-macos-build) of this repository.
To get access to the Circle-CI build administration, packagers need to
ask admin access to this Github repository.

## Before you start
The GTK and SiriL build processes on macOS are very fragile. If you have any other build system (brew, MacPorts) installed – the local build instructions provide some support (taking `Homebrew` off `PATH`. Try to remove or isolate them from the JHBuild environment as much as you can.

The main reason for this: everything that SiriL needs must be packaged in the executable bundle or be part
of the MacOS SDK that is being called.

Some people were able to get working builds in the VirtualBox VM, others in a VMWare Fusion VM. Another approach could
be to create a separate user on your Mac.

In any case, the build process on Circle CI or the local version (see below) sets up most things from scratch.

## Prerequisites for local build (Draft) ##

At a minimum, you will need to install:

- XCode Command Line Tools
- [Rust](https://www.rust-lang.org/tools/install) (don't use `Homebrew` or `MacPorts`).

## Steps in the CircleCI [config.yml](https://gitlab.gnome.org/Infrastructure/gimp-macos-build/blob/master/.circleci/config.yml) are:
- Install Python 3 (Rust is pre-installed) as they are required for the GIMP dependencies.
- Set up macOS 10.12 SDK. This is needed to ensure that SiriL can run on macOS 10.12+. See [this article](https://smallhacks.wordpress.com/2018/11/11/how-to-support-old-osx-version-with-a-recent-xcode/) for the details.
- Set up JHBuild with a custom `~/.config/jhbuildrc-custom` file (see https://github.com/GNOME/gimp-macos-build/blob/master/jhbuildrc-gtk-osx-gimp-2.99). As part of the setup, it is running `bootstrap-gtk-osx-gimp` JHBuild command to compile required modules to run JHBuild. JHBuild is using Python3 venv to run.
- Install [fork of the gtk-mac-bundler](https://github.com/lukaso/gtk-mac-bundler) - the tool which helps to create macOS application bundles for the GTK apps. This will hopefully shift to official [gtk-mac-bundler](https://github.com/GNOME/gtk-mac-bundler)
- Installing all gtk-osx and SiriL dependencies using JHBuild
- Building SiriL (from the git).
- Launch `build.sh` which does (among other things):
  - Build package using `gtk-mac-bundler`
  - Use `install_name_tool` to fix all library paths to make package relocatable.
  - generate debug symbols
  - fix `pixmap` and `imm` cache files to remove absolute pathnames
  - fix `.gir` and `.typelib` library paths to make package relocatable
  - copy in icons
  - Sign all binaries
  - Create a DMG package using [create-dmg](https://github.com/andreyvit/create-dmg) tool and sign it
- Notarize package using Apple `altool` utility
- Upload a DMG to the CircleCI build artifacts

## Managing the Circle CI build ##

The Circle CI build and its interaction with JHBuild create some specific issues that a packager needs to be aware of.

### Build timelimit ###

The builds have a strict time limit of 1 hour. As soon as the build takes longer, it is canceled.

Due to this, and the fact that the build as a whole takes much more than an hour, creative measures
have had to be taken.

**Note** There is a hard limit on the length of a single build step. If a step takes longer than an hour it simply
can't be build in Circle CI.

### JHBuild not detecting changes ###

Because of JHBuild's architecture, certain changes to packages it is building, are not detected at build time. This means that the build can become out of date and a full cache-break build will have to be undertaken.

Examples of things that JHBuild does not detect:

- A new patch file being added
- Changes to environment variables (such a `CFLAGS`)
- Changes to the build command

Examples of things that JHBuild does detect:

- A new URL/version of a package
- New commits on a git based repo
- Changes in a dependency

### Caching to get around build timelimit ###

In order to get around the build time limit, each step of the build (or sometimes group of steps) is cached. This
uses Circleci's caching mechanism.

The following are aspects of the caching:

- The JHBuild cache and the rest of the build cache are managed separately
- The cache is only saved when a new cache key is used, so by default, nothing new that happens in a build is saved until the cache key is Other

## Other related links

 - [Gtk-OSX](https://gitlab.gnome.org/GNOME/gtk-osx/) project to simplify building MacOS application bundles for Gtk+-based applications
 - CircleCI [siril-macos-build project](https://circleci.com/gh/lock042/siril-macos-build)
 - How this repo uses [JHBuild and Gtk-OSX](README_JHBUILD_GTK_OSX.md)
